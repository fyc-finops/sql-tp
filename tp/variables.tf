variable "name_prefix" {
  default     = "postgresqlfs"
  description = "Prefix of the resource name."
}

variable "location" {
  default     = "eastus"
  description = "Location of the resource."
}

variable "storage_server" {
  default     = 32768
  type        = string
  description = "Storage number"
}

variable "sku_name" {
  default     = "GP_Standard_D2s_v3"
  type        = string
  description = "Sku name"
}

variable "retention_days" {
  default     = 7
  type        = string
  description = "How many times you want to keep the data"
}

variable "postgresql_version" {
  default     = 13
  type        = string
  description = "Version of the PostgreSQL Server"
}

variable "area" {
  default     = 1
  type        = string
  description = "Default area in Azure"
}

variable "login" {
  default     = "adminTerraform"
  type        = string
  description = "Default Login for the server"
}
